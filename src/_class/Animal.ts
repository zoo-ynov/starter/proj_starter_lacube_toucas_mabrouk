

export class Animal
{
   id          : number; 
   type        : string;
   enclosId    : number;
   age         ?: number; 
}