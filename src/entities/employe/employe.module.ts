import { Module } from '@nestjs/common';
import { EmployeService } from './services/employe.service';
import { EmployeController } from './controllers/employe.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employe } from './employe.entity';

@Module({
   imports: [
      TypeOrmModule.forFeature([
         Employe
      ]),
    ],
  providers: [EmployeService],
  controllers: [EmployeController]
})
export class EmployeModule {}
