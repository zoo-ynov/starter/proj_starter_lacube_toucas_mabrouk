import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm';
import { Zoo } from '../zoo/zoo.entity';
import { ApiModelProperty } from '@nestjs/swagger';

@Entity()
export class Employe
{
   @ApiModelProperty()
   @PrimaryGeneratedColumn()
   id: number;

   @ApiModelProperty()
   @Column()
   lastName: string;

   @ApiModelProperty()
   @Column()
   firstName: string;

   @ApiModelProperty()
   @Column()
   location: string;

   @ApiModelProperty()
   @ManyToOne(type => Zoo, zoo => zoo.employe)
   zoo: Zoo
}