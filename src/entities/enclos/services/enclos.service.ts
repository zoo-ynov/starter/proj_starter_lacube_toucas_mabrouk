import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Enclos } from '../enclos.entity';
import { Repository, UpdateResult, DeleteResult } from 'typeorm';

@Injectable()
export class EnclosService {
   constructor(
      @InjectRepository(Enclos)
      private zooRepository: Repository<Enclos>,
  ) { }

  async  findOne(id: number): Promise<Enclos>
   {
      return await this.zooRepository.findOne(id);
   }

  async  findAll(): Promise<Enclos[]>
   {
      return await this.zooRepository.find();
   }

   async  create(zoo: Enclos): Promise<Enclos>
   {
      return await this.zooRepository.save(zoo);
   }

   async update(zoo: Enclos): Promise<UpdateResult>
   {
      return await this.zooRepository.update(zoo.id, zoo);
   }

   async delete(id): Promise<DeleteResult>
   {
      return await this.zooRepository.delete(id);
   }

}
