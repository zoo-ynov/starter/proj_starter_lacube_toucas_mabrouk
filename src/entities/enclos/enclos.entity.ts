import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm';
import { Zoo } from '../zoo/zoo.entity';
import { Animaux } from '../animaux/animaux.entity';
import { ApiModelProperty } from '@nestjs/swagger';


@Entity()
export class Enclos
{
   @ApiModelProperty()
   @PrimaryGeneratedColumn()
   id: number;

   @ApiModelProperty()
   @Column()
   name: string;

   @ApiModelProperty()
   @Column()
   location: string;

   @ApiModelProperty()
   @Column()
   tailleMaxAnimaux: string;

   @ApiModelProperty()
   @ManyToOne(type => Zoo, zoo => zoo.enclos)
   zoo: Zoo

   @ApiModelProperty()
   @OneToMany(type => Animaux, Animaux => Animaux.enclos) // note: we will create author property in the Photo class below
   animaux: Animaux[];
}