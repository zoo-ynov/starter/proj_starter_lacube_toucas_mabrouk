import { Module } from '@nestjs/common';
import { AnimauxController } from './controllers/animaux.controller';
import { AnimauxService } from './services/animaux.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Animaux } from './animaux.entity';

@Module({
   imports: [
      TypeOrmModule.forFeature([
         Animaux
      ]),
    ],
  controllers: [AnimauxController],
  providers: [AnimauxService]
})
export class AnimauxModule {}
