import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Animaux } from '../animaux.entity';
import { Repository, UpdateResult, DeleteResult } from 'typeorm';

@Injectable()
export class AnimauxService {
   constructor(
      @InjectRepository(Animaux)
      private zooRepository: Repository<Animaux>,
  ) { }

  async  findAll(): Promise<Animaux[]>
   {
      return await this.zooRepository.find();
   }

   async  findOne(id: number): Promise<Animaux>
   {
      return await this.zooRepository.findOne(id);
   }

   async  create(zoo: Animaux): Promise<Animaux>
   {
      return await this.zooRepository.save(zoo);
   }

   async update(zoo: Animaux): Promise<UpdateResult>
   {
      return await this.zooRepository.update(zoo.id, zoo);
   }

   async delete(id): Promise<DeleteResult>
   {
      return await this.zooRepository.delete(id);
   }
}
