import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ZooModule } from './entities/zoo/zoo.module';
import { EmployeModule } from './entities/employe/employe.module';
import { EnclosModule } from './entities/enclos/enclos.module';
import { AnimauxModule } from './entities/animaux/animaux.module';
import { ZooService } from './entities/zoo/services/zoo.service';
import { Connection } from 'typeorm';
import { BootstrapController } from './bootstrap/bootstrap.controller';

@Module({
  imports: [
   TypeOrmModule.forRoot(),
   ZooModule,
   EmployeModule,
   EnclosModule,
   AnimauxModule
  ],
  controllers: [BootstrapController],
  providers: [],
})
export class AppModule {
   constructor(public readonly connection: Connection){}
}
