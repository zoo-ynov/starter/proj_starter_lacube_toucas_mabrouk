<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

# Introduction
## Projet niveau "Starter"
<p>Nous avons choisi le le projet niveau "starter" car c'est la première fois que nous devellopons, pour tout le groupe, une api node utilisant Nest et TypeORM.</p>

# Installation

## First execution

```bash
$ npm install
```

- Lancer une base de données PostgreSql en local
- Changer le ficher ormconfig.json à la racine
- Y renseigner les informations propres à votre base de données PostGreSQL

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

Nous avons réalisé les test e2e

```bash
# unit tests jest and e2e
$ npm run test

# jest test
$ npm run test:jest

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Document de l'API : Swagger

Pour accéder à la documentation de l'api:
http://localhost:3000/api


## Groupe

- Romain Lacube (romain.lacube@ynov.com)
- Cyprien Toucas (cyprien.toucas@ynov.com)
- Radhoine Mabrouk (radhoine.mabrouk@ynov.com)
